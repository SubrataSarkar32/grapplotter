#Graph plotter 2.0 for visualization purpose
#made by Subrata Sarkar(subrotosarkar32@gmail.com)
flag=0
try:
    import numpy as np
    from matplotlib import pyplot as plt

except:
    print("numpy and matplotlib lib not found . Installing them")
    flag=1
if flag==1:
    try:
        from pip import main as pipmain
    except ImportError:
        from pip._internal import main as pipmain
    pipmain(['install', 'numpy', 'matplotlib']) 
        
import sys, os
def plot(lines=0,line_list=[]):
    datlist=[]
    

    if sys.version_info[0] < 3:
        print("Port to Python 3 or a more recent version.")
        t = lines
        color=iter(plt.cm.rainbow(np.linspace(0,1,t)))
        for j in range(t):
            print(line_list)
            datlist = line_list[j]
            data=np.array(datlist)
            x,y=data.T
            c=next(color)
            plt.plot(x,y,c=c)
            plt.scatter(x,y,c=c)
        plt.show()
        print("Complted plotting")
        
    else:
        t = lines
        color=iter(plt.cm.rainbow(np.linspace(0,1,t)))
        for j in range(t):
            datlist = line_list[t]
            data=np.array(datlist)
            x,y = data.T
            c=next(color)
            plt.plot(x,y,c=c)
            plt.scatter(x,y,c=c)
        plt.show()
        print("Complted plotting")
def caller_all_files():
    '''This function automatically runs graph plotter on all files present in this directory'''
    PATH = os.path.abspath(os.path.dirname(__file__))
    pdfFiles = []
    for filename in os.listdir('.'):
        if filename.endswith('.txt'):
            filepath=os.path.join(PATH, filename)
            pdfFiles.append(filepath)
    for filey in pdfFiles:
        print("Analyzing ... ",filey)
        file1=open(filey,mode='r')
        str1=file1.read()
        strlst=str1.split('\n')
        lines=0
        lines=int(strlst[0])
        strlst.pop(0)
        list2=[]
        for i in range(lines):
            listy=tuple(map(int,strlst[i].split()))
            listy2=[(listy[x],listy[x+1]) for x in range(0,len(listy)-1,2)] 
            list2+=[listy2]
        plot(lines,list2)
def caller_file(filey=''):
        '''This function runs graph plotter on the file name provided
           File should of the form:
           n
           x00 y00 x01 y01
           x10 y10 x11 y11
           x20 y20 x21 y21'''
        print("Analyzing ... ",filey)
        file1=open(filey,mode='r')
        str1=file1.read()
        strlst=str1.split('\n')
        lines=0
        lines=int(strlst[0])
        strlst.pop(0)
        list2=[]
        for i in range(lines):
            listy=tuple(map(int,strlst[i].split()))
            listy2=[(listy[x],listy[x+1]) for x in range(0,len(listy)-1,2)] 
            list2+=[listy2]
        plot(lines,list2)
print("Graph plotter")
if len(sys.argv)>1:
    for uio in range(1,len(sys.argv)):
        fname=os.path.abspath(sys.argv[uio])
        caller_file(filey=fname)
        
