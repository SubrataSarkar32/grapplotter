#Graph plotter 2.0 for visualization purpose
#made by Subrata Sarkar(subrotosarkar32@gmail.com)
flag=0
try:
    import numpy as np
    from matplotlib import pyplot as plt

except:
    print("numpy and matplotlib lib not found . Installing them")
    flag=1
if flag==1:
    try:
        from pip import main as pipmain
    except ImportError:
        from pip._internal import main as pipmain
    pipmain(['install', 'numpy', 'matplotlib'])

print("Graph plotter")
ch1=1
while ch1==1:
    datlist=[]
    import sys

    if sys.version_info[0] < 3:
        print("Port to Python 3 or a more recent version.")
        t = int(raw_input("Enter number of lines you want to plot: "))
        color=iter(plt.cm.rainbow(np.linspace(0,1,t)))
        for j in range(t):
            a=int(raw_input("Enter number of entries you want to plot: "))
            datlist=[]
            for i in range(a):
              x=float(raw_input("Enter value of x: "))
              y=float(raw_input("Enter value of y: "))
              datlist+=[[x,y]]
            data=np.array(datlist)
            x,y=data.T
            c=next(color)
            plt.plot(x,y,c=c)
            plt.scatter(x,y,c=c)
        plt.show()
        print("Complted plotting")
        ch1=int(raw_input("Do you want to continue(1-Yes/0-No)?"))
        while (ch1!=0) and (ch1!=1):
            ch1=int(raw_input("Do you want to continue(1-Yes/0-No)?"))
    else:
        t = int(input("Enter number of lines you want to plot: "))
        color=iter(plt.cm.rainbow(np.linspace(0,1,t)))
        for j in range(t):
            a=int(input("Enter number of entries you want to plot: "))
            datlist=[]
            for i in range(a):
              x=float(input("Enter value of x: "))
              y=float(input("Enter value of y: "))
              datlist+=[[x,y]]
            data=np.array(datlist)
            x,y=data.T
            c=next(color)
            plt.plot(x,y,c=c)
            plt.scatter(x,y,c=c)
        plt.show()
        print("Complted plotting")
        ch1=int(input("Do you want to continue(1-Yes/0-No)?"))
        while (ch1!=0) and (ch1!=1):
            ch1=int(input("Do you want to continue(1-Yes/0-No)?")) 
