import glob
import os
import sys
from distutils.core import setup
import py2exe
import enchant

py2exe.build_exe._py_suffixes = ['.py', '.pyo', '.pyc', '.pyw']

def files(folder):
     for path in glob.glob(folder+'/*'):
          if os.path.isfile(path):
              yield path
              
              
from distutils.filelist import findall
import os
import matplotlib
matplotlibdatadir = matplotlib.get_data_path()
matplotlibdata = findall(matplotlibdatadir)
matplotlibdata_files = []
for f in matplotlibdata:
    dirname = os.path.join('matplotlibdata', f[len(matplotlibdatadir)+1:])
    matplotlibdata_files.append((os.path.split(dirname)[0], [f]))

   

setup(
       version = "1.0",            
       name = "Alice",
       url = "",
       author = "Subrata Sarkar",
       author_email = "subrotosarkar32@gmail.com",
       license = "apachev2 License",
       
       console=[{'script': "graphplot.py",'copyright': "Copyright (c) 2018 Subrata Sarkar"}] ,data_files=matplotlibdata_files,options={'py2exe':{"skip_archive": True,"unbuffered": True,'dll_excludes': ['libgdk-win32-2.0-0.dll','libgdk_pixbuf-2.0-0.dll','libgobject-2.0-0.dll'],'excludes':['jinja2'],'packages':['Tkinter','numpy','matplotlib', 'pytz']}}
     )
